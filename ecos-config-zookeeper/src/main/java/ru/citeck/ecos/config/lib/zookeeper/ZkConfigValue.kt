package ru.citeck.ecos.config.lib.zookeeper

import ru.citeck.ecos.commons.data.DataValue

data class ZkConfigValue(
    val value: DataValue,
    val allowedFor: List<String>
)
