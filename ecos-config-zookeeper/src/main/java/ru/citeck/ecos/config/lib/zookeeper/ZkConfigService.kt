package ru.citeck.ecos.config.lib.zookeeper

import ecos.curator.org.apache.zookeeper.KeeperException
import ecos.curator.org.apache.zookeeper.WatchedEvent
import ecos.curator.org.apache.zookeeper.Watcher
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.utils.NameUtils
import ru.citeck.ecos.config.lib.dto.ConfigKey
import ru.citeck.ecos.config.lib.dto.ConfigValue
import ru.citeck.ecos.config.lib.dto.event.ConfigEvent
import ru.citeck.ecos.config.lib.dto.event.ConfigEventType
import ru.citeck.ecos.config.lib.provider.EcosConfigMutableProvider
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory
import ru.citeck.ecos.zookeeper.EcosZooKeeper

class ZkConfigService(
    ecosZooKeeper: EcosZooKeeper,
    services: EcosConfigServiceFactory
) : EcosConfigMutableProvider {

    private val ecosZooKeeper = ecosZooKeeper.withNamespace("ecos/config")
    private val configIdEscaper = NameUtils.getEscaper("/")

    private val defaultScope = services.properties.defaultScope

    override fun getConfig(key: ConfigKey): ConfigValue? {
        return getZkConfig(key)?.let { ConfigValue(it.value, it.allowedFor) }
    }

    private fun getZkConfig(key: ConfigKey): ZkConfigValue? {
        return try {
            ecosZooKeeper.getValue(toZkPath(key), ZkConfigValue::class.java)
        } catch (e: KeeperException.NoNodeException) {
            null
        }
    }

    override fun setConfig(key: ConfigKey, value: Any?) {
        val dataValue = DataValue.create(value)
        val newValue = ZkConfigValue(dataValue, emptyList())
        ecosZooKeeper.setValue(toZkPath(key), newValue)
    }

    override fun remove(key: ConfigKey) {
        ecosZooKeeper.deleteValue(toZkPath(key))
    }

    override fun watch(action: (ConfigEvent) -> Unit) {
        ecosZooKeeper.watchChildrenRecursive("/") {
            processEvent(it, action)
        }
    }

    private fun processEvent(event: WatchedEvent?, listener: (ConfigEvent) -> Unit) {

        event ?: return

        val delimitersCount = event.path?.count { it == '/' } ?: 0
        if (delimitersCount <= 1) {
            return
        }

        val configEventType = if (event.type == Watcher.Event.EventType.NodeDeleted) {
            ConfigEventType.DELETED
        } else {
            ConfigEventType.CHANGED
        }

        val scope = event.path.substringBeforeLast('/').substring(1)
        val id = event.path.substringAfterLast('/')
        val key = ConfigKey.create(scope, configIdEscaper.unescape(id))

        listener.invoke(ConfigEvent(key, configEventType))
    }

    private fun toZkPath(key: ConfigKey): String {
        val normalizedKey = key.withDefaultScope(defaultScope)
        return "/" + normalizedKey.scope + "/" + configIdEscaper.escape(normalizedKey.id)
    }

    override fun getOrder() = 0f
}
