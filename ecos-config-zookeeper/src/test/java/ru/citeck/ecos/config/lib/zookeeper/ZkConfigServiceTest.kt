package ru.citeck.ecos.config.lib.zookeeper

import ecos.org.apache.curator.RetryPolicy
import ecos.org.apache.curator.framework.CuratorFrameworkFactory
import ecos.org.apache.curator.retry.RetryForever
import ecos.org.apache.curator.test.TestingServer
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import ru.citeck.ecos.config.lib.consumer.bean.EcosConfig
import ru.citeck.ecos.config.lib.dto.ConfigKey
import ru.citeck.ecos.config.lib.dto.EcosConfigProperties
import ru.citeck.ecos.config.lib.provider.EcosConfigProvider
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory
import ru.citeck.ecos.zookeeper.EcosZooKeeper

class ZkConfigServiceTest {

    @Test
    fun test() {

        val zkServer = TestingServer()

        val retryPolicy: RetryPolicy = RetryForever(7_000)

        val client = CuratorFrameworkFactory
            .newClient(zkServer.connectString, retryPolicy)
        client.start()

        val ecosZooKeeper = EcosZooKeeper(client)

        val services = object : EcosConfigServiceFactory() {
            override fun createProperties(): EcosConfigProperties {
                val props = super.createProperties()
                props.defaultScope = "app/app_name"
                return props
            }

            override fun createEcosConfigProviders(): List<EcosConfigProvider> {
                return listOf(
                    ZkConfigService(ecosZooKeeper, this)
                )
            }
        }
        val zkConfigService = services.ecosConfigService.getProvider(ZkConfigService::class.java)!!

        val dto = TestDto()
        val consumersContext = services.beanConsumersService.registerConsumers(dto)

        val setZkValue = { key: String, value: Any ->
            zkConfigService.setConfig(ConfigKey.create("app/app_name", key), value)
            Thread.sleep(300)
        }

        setZkValue("some-config", "test-value")
        assertThat(dto.config).isEqualTo("test-value")

        setZkValue("some-config", "test-value2")
        assertThat(dto.config).isEqualTo("test-value2")

        assertThat(dto.valueSet).isEmpty()
        val newElements = listOf("first", "second")
        setZkValue("valueSet", newElements)
        assertThat(dto.valueSet).containsExactlyInAnyOrderElementsOf(newElements)

        consumersContext.remove()

        setZkValue("some-config", "test-value3")
        assertThat(dto.config).isEqualTo("test-value2")
    }

    class TestDto {

        @EcosConfig("some-config")
        var config: String? = null

        @EcosConfig("valueSet")
        var valueSet: Set<String> = emptySet()
    }
}
