package ru.citeck.ecos.config.lib.records

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.citeck.ecos.config.lib.provider.InMemConfigService
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory
import ru.citeck.ecos.records3.RecordsService
import ru.citeck.ecos.records3.RecordsServiceFactory

class EcosConfigRecordsTest {

    lateinit var records: RecordsService
    lateinit var inMemConfigService: InMemConfigService

    @BeforeEach
    fun beforeEach() {
        val services = EcosConfigServiceFactory()
        val recordsServices = RecordsServiceFactory()
        records = recordsServices.recordsServiceV1
        records.register(CfgRecordsDao(services))
        recordsServices.ctxAttsService.register(CfgCtxAttsProvider(services))
        inMemConfigService = services.inMemConfigService
    }

    @Test
    fun test() {
        val value = "123"
        inMemConfigService.setConfig("test-config", value)

        val ctxAttValue = records.getAtt("", "\$cfg.test-config").asText()
        assertThat(ctxAttValue).isEqualTo(value)

        val recordAttValue = records.getAtt("cfg@test-config", "value").asText()
        assertThat(recordAttValue).isEqualTo(value)
    }
}
