package ru.citeck.ecos.config.lib.records

import ru.citeck.ecos.config.lib.service.EcosConfigService
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory
import ru.citeck.ecos.records3.record.atts.value.AttValue
import ru.citeck.ecos.records3.record.request.ctxatts.CtxAttsProvider
import ru.citeck.ecos.records3.record.request.ctxatts.StdCtxAttsProvider

class CfgCtxAttsProvider(services: EcosConfigServiceFactory) : CtxAttsProvider {

    companion object {
        const val CFG_ATT = "cfg"
    }

    private val value = CfgAttValue(services.ecosConfigService)

    override fun fillContextAtts(attributes: MutableMap<String, Any?>) {
        attributes[CFG_ATT] = value
    }

    private class CfgAttValue(val service: EcosConfigService) : AttValue {
        override fun getAtt(name: String): Any {
            return service.getConfig(name)
        }
    }

    override fun getOrder() = StdCtxAttsProvider.ORDER
}
