package ru.citeck.ecos.config.lib.records

import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory
import ru.citeck.ecos.records3.record.atts.value.AttValue
import ru.citeck.ecos.records3.record.dao.AbstractRecordsDao
import ru.citeck.ecos.records3.record.dao.atts.RecordAttsDao

class CfgRecordsDao(services: EcosConfigServiceFactory) : AbstractRecordsDao(), RecordAttsDao {

    companion object {
        const val ID = "cfg"
    }

    private val configService = services.ecosConfigService

    override fun getRecordAtts(recordId: String): Any {
        return ConfigValue(recordId)
    }

    override fun getId() = ID

    inner class ConfigValue(private val key: String) : AttValue {
        override fun getAtt(name: String): Any? {
            if (name == "value") {
                return configService.getConfig(key)
            }
            return null
        }
    }
}
