package ru.citeck.ecos.config.lib.dto.event

enum class ConfigEventType {
    CHANGED,
    DELETED
}
