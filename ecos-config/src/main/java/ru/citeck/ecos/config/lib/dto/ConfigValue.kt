package ru.citeck.ecos.config.lib.dto

import ru.citeck.ecos.commons.data.DataValue

data class ConfigValue(
    val value: DataValue,
    val allowedFor: List<String>
)
