package ru.citeck.ecos.config.lib.service

import ru.citeck.ecos.config.lib.consumer.bean.BeanConsumerService
import ru.citeck.ecos.config.lib.consumer.bean.BeanConsumerServiceImpl
import ru.citeck.ecos.config.lib.dto.EcosConfigProperties
import ru.citeck.ecos.config.lib.provider.EcosConfigProvider
import ru.citeck.ecos.config.lib.provider.InMemConfigService

open class EcosConfigServiceFactory {

    val properties by lazy { createProperties() }
    val ecosConfigService by lazy { createEcosConfigService() }
    val beanConsumersService by lazy { createBeanConsumersService() }
    val ecosConfigProviders by lazy { createEcosConfigProviders() }
    val inMemConfigService by lazy { createInMemConfigService() }

    protected open fun createProperties(): EcosConfigProperties {
        return EcosConfigProperties()
    }

    protected open fun createEcosConfigService(): EcosConfigService {
        return EcosConfigServiceImpl(this)
    }

    protected open fun createBeanConsumersService(): BeanConsumerService {
        return BeanConsumerServiceImpl(this)
    }

    protected open fun createEcosConfigProviders(): List<EcosConfigProvider> {
        return emptyList()
    }

    protected open fun createInMemConfigService(): InMemConfigService {
        return InMemConfigService(this)
    }
}
