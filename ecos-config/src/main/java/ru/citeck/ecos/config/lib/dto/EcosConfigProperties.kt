package ru.citeck.ecos.config.lib.dto

data class EcosConfigProperties(
    var defaultScope: String? = null
)
