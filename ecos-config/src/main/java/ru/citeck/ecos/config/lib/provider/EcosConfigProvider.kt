package ru.citeck.ecos.config.lib.provider

import ru.citeck.ecos.config.lib.dto.ConfigKey
import ru.citeck.ecos.config.lib.dto.ConfigValue
import ru.citeck.ecos.config.lib.dto.event.ConfigEvent

interface EcosConfigProvider {

    fun getConfig(key: String): ConfigValue? {
        return getConfig(ConfigKey.valueOf(key))
    }

    fun getConfig(key: ConfigKey): ConfigValue?

    fun watch(action: (ConfigEvent) -> Unit)

    fun getOrder(): Float
}
