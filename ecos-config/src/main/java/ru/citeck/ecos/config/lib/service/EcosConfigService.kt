package ru.citeck.ecos.config.lib.service

import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.config.lib.consumer.context.ConsumersContext
import ru.citeck.ecos.config.lib.dto.ConfigKey
import ru.citeck.ecos.config.lib.provider.EcosConfigProvider

interface EcosConfigService {

    fun getConfig(key: String): DataValue

    fun getConfig(key: ConfigKey): DataValue

    fun registerConsumer(key: ConfigKey, consumer: (DataValue) -> Unit): ConsumersContext

    fun registerConsumers(consumers: Map<ConfigKey, List<(DataValue) -> Unit>>): ConsumersContext

    fun <T : EcosConfigProvider> getProvider(type: Class<T>): T?
}
