package ru.citeck.ecos.config.lib.service

import mu.KotlinLogging
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.config.lib.consumer.context.ConsumersContext
import ru.citeck.ecos.config.lib.dto.ConfigKey
import ru.citeck.ecos.config.lib.dto.ConfigValue
import ru.citeck.ecos.config.lib.dto.event.ConfigEvent
import ru.citeck.ecos.config.lib.provider.EcosConfigProvider
import ru.citeck.ecos.context.lib.auth.AuthConstants
import ru.citeck.ecos.context.lib.auth.AuthContext
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList

class EcosConfigServiceImpl(services: EcosConfigServiceFactory) : EcosConfigService {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    private val properties = services.properties
    private val providers: List<EcosConfigProvider> by lazy {

        val providers = ArrayList(services.ecosConfigProviders)
        providers.add(services.inMemConfigService)
        providers.sortBy { it.getOrder() }

        providers.forEach { provider ->
            provider.watch {
                AuthContext.runAsSystem {
                    onConfigChanged(it)
                }
            }
        }
        providers
    }

    private val consumers: MutableMap<ConfigKey, MutableList<(DataValue) -> Unit>> = ConcurrentHashMap()
    private val configValues = ConcurrentHashMap<ConfigKey, DataValue>()

    private val consumersConfigValues = ConcurrentHashMap<ConfigKey, DataValue>()

    @Synchronized
    private fun onConfigChanged(event: ConfigEvent) {
        val key = event.key.withDefaultScope(properties.defaultScope)
        val newValue = updateConfigAndGet(key)
        val prevValue = consumersConfigValues[key] ?: DataValue.NULL
        if (newValue == prevValue) {
            return
        }
        log.info { "Config was changed. Key: $key. Value: $prevValue -> $newValue" }
        consumers[key]?.forEach {
            try {
                it.invoke(newValue)
            } catch (e: Throwable) {
                log.error(e) { "Config value can't be changed. Key: $key. Value: $prevValue -> $newValue" }
            }
        }
        consumersConfigValues[key] = newValue
    }

    override fun getConfig(key: String): DataValue {
        return getConfig(ConfigKey.valueOf(key))
    }

    override fun getConfig(key: ConfigKey): DataValue {
        return configValues.computeIfAbsent(key) { evalConfig(it) }
    }

    private fun updateConfigAndGet(key: ConfigKey): DataValue {
        configValues.remove(key)
        return configValues.computeIfAbsent(key) { evalConfig(it) }
    }

    private fun evalConfig(key: ConfigKey): DataValue {
        val configKey = key.withDefaultScope(properties.defaultScope)
        val authorities = AuthContext.getCurrentRunAsUserWithAuthorities()
        for (provider in providers) {
            val provConfig = provider.getConfig(configKey)
            if (provConfig != null) {
                if (!isAllowed(provConfig, authorities)) {
                    return DataValue.NULL
                }
                return provConfig.value
            }
        }
        return DataValue.NULL
    }

    @Synchronized
    override fun registerConsumers(consumers: Map<ConfigKey, List<(DataValue) -> Unit>>): ConsumersContext {

        val contextConsumers = HashMap<ConfigKey, List<(DataValue) -> Unit>>()

        AuthContext.runAsSystem {
            consumers.forEach { (key, keyConsumers) ->

                val configValue = getConfig(key)
                keyConsumers.forEach { it.invoke(configValue) }
                consumersConfigValues[key] = configValue

                val serviceConsumers = this.consumers.computeIfAbsent(key) { CopyOnWriteArrayList() }
                serviceConsumers.addAll(keyConsumers)
                contextConsumers[key] = ArrayList(keyConsumers)
            }
        }

        return ConsumersServiceContext(contextConsumers)
    }

    override fun registerConsumer(key: ConfigKey, consumer: (DataValue) -> Unit): ConsumersContext {
        return registerConsumers(mapOf(key to listOf(consumer)))
    }

    private fun isAllowed(value: ConfigValue, authorities: List<String>): Boolean {
        if (value.allowedFor.isEmpty()) {
            return true
        }
        if (authorities.contains(AuthConstants.SYSTEM_USER)) {
            return true
        }
        return value.allowedFor.any { authorities.contains(it) }
    }

    override fun <T : EcosConfigProvider> getProvider(type: Class<T>): T? {
        @Suppress("UNCHECKED_CAST")
        return providers.firstOrNull { it::class.java.isAssignableFrom(type) } as? T
    }

    private inner class ConsumersServiceContext(
        private val ctxConsumers: Map<ConfigKey, List<(DataValue) -> Unit>>
    ) : ConsumersContext {

        private val size = ctxConsumers.values.sumOf { it.size }
        private var removed = false

        override fun remove() {
            if (removed) {
                return
            }
            ctxConsumers.forEach { (key, keyConsumers) ->
                consumers[key]?.removeIf { serviceConsumer -> keyConsumers.any { it === serviceConsumer } }
            }
            removed = true
        }

        override fun size() = size
    }
}
