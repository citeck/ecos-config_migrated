package ru.citeck.ecos.config.lib.consumer.bean

@Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FIELD,
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY,
    AnnotationTarget.PROPERTY_SETTER
)
annotation class EcosConfig(
    /**
     * Config ID
     *
     * Default scope is "app/{CURRENT_APP_NAME}"
     *
     * Examples:
     * "abc" -> { scope: DEFAULT, id: "abc" }
     * "abc$def" -> { scope: "abc", id: "def" }
     */
    val value: String
)
