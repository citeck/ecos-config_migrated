package ru.citeck.ecos.config.lib.consumer.context

interface ConsumersContext {
    /**
     * Remove consumers owned by this context
     */
    fun remove()

    fun size(): Int
}
