package ru.citeck.ecos.config.lib.consumer.context

object ConsumersEmptyContext : ConsumersContext {

    override fun remove() {
    }

    override fun size(): Int = 0
}
