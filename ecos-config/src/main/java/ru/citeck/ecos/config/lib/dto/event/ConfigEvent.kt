package ru.citeck.ecos.config.lib.dto.event

import ru.citeck.ecos.config.lib.dto.ConfigKey

data class ConfigEvent(
    val key: ConfigKey,
    val type: ConfigEventType
)
