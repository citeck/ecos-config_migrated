package ru.citeck.ecos.config.lib.dto

class ConfigKey private constructor(
    val scope: String,
    val id: String
) {

    companion object {

        const val SCOPE_DELIM = "$"

        val EMPTY = ConfigKey("", "")

        @JvmStatic
        fun valueOf(value: String?): ConfigKey {
            if (value.isNullOrBlank()) {
                return EMPTY
            }
            return when (val delimIdx = value.indexOf(SCOPE_DELIM)) {
                -1 -> create(value)
                0 -> create(value.substring(1))
                value.length - 1 -> create(value.substring(0, value.length - 1), "")
                else -> create(value.substring(0, delimIdx), value.substring(delimIdx + 1))
            }
        }

        @JvmStatic
        fun create(id: String?): ConfigKey {
            return create("", id)
        }

        @JvmStatic
        fun create(scope: String?, id: String?): ConfigKey {
            val notNullId = toNotNulLStr(id)
            val notNullScope = toNotNulLStr(scope)
            if (notNullId.isNotEmpty() || notNullScope.isNotEmpty()) {
                return ConfigKey(notNullScope, notNullId)
            }
            return EMPTY
        }

        private fun toNotNulLStr(value: String?): String {
            if (value.isNullOrBlank()) {
                return ""
            }
            return if (value.startsWith(' ') || value.endsWith(' ')) {
                value.trim()
            } else {
                value
            }
        }
    }

    fun withScope(scope: String?): ConfigKey {
        if (this.scope == scope) {
            return this
        }
        return create(scope, this.id)
    }

    fun withDefaultScope(scope: String?): ConfigKey {
        if (scope.isNullOrBlank() || this.scope.isNotEmpty()) {
            return this
        }
        return create(scope, this.id)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (javaClass != other?.javaClass) {
            return false
        }
        other as ConfigKey
        if (scope != other.scope) {
            return false
        }
        if (id != other.id) {
            return false
        }
        return true
    }

    override fun hashCode(): Int {
        var result = scope.hashCode()
        result = 31 * result + id.hashCode()
        return result
    }

    override fun toString(): String {
        return if (this == EMPTY) {
            ""
        } else if (scope.isEmpty()) {
            if (id.contains('$')) {
                "$SCOPE_DELIM$id"
            } else {
                id
            }
        } else {
            "$scope$SCOPE_DELIM$id"
        }
    }
}
