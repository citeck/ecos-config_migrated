package ru.citeck.ecos.config.lib.consumer.bean

import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.config.lib.consumer.context.ConsumersContext
import ru.citeck.ecos.config.lib.dto.ConfigKey

interface BeanConsumerService {

    fun registerConsumers(bean: Any?): ConsumersContext

    fun unregisterConsumers(bean: Any?)

    fun getConsumers(bean: Any?): Map<ConfigKey, List<(DataValue) -> Unit>>
}
