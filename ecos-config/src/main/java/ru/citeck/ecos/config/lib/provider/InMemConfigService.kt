package ru.citeck.ecos.config.lib.provider

import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.config.lib.dto.ConfigKey
import ru.citeck.ecos.config.lib.dto.ConfigValue
import ru.citeck.ecos.config.lib.dto.event.ConfigEvent
import ru.citeck.ecos.config.lib.dto.event.ConfigEventType
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList

/**
 * Config service with map based config storage.
 *
 * Warning! All config values will be deleted when application will be stopped.
 *
 * The main purpose of this service is using in unit tests
 */
class InMemConfigService(services: EcosConfigServiceFactory) : EcosConfigMutableProvider {

    private val configs = ConcurrentHashMap<ConfigKey, ConfigValue>()

    private val listeners = CopyOnWriteArrayList<(ConfigEvent) -> Unit>()
    private val defaultScope = services.properties.defaultScope

    override fun getConfig(key: ConfigKey): ConfigValue? {
        return configs[key.withDefaultScope(defaultScope)]
    }

    override fun setConfig(key: ConfigKey, value: Any?) {
        val normalizedKey = key.withDefaultScope(defaultScope)
        val valueBefore = configs[normalizedKey]
        configs[normalizedKey] = ConfigValue(DataValue.create(value), emptyList())
        try {
            listeners.forEach {
                it.invoke(ConfigEvent(normalizedKey, ConfigEventType.CHANGED))
            }
        } catch (e: Throwable) {
            if (valueBefore == null) {
                configs.remove(normalizedKey)
            } else {
                configs[normalizedKey] = valueBefore
            }
            throw e
        }
    }

    override fun remove(key: ConfigKey) {
        val normalizedKey = key.withDefaultScope(defaultScope)
        val value = configs.remove(normalizedKey)
        try {
            listeners.forEach {
                it.invoke(ConfigEvent(normalizedKey, ConfigEventType.DELETED))
            }
        } catch (e: Throwable) {
            if (value != null) {
                configs[normalizedKey] = value
            }
            throw e
        }
    }

    override fun watch(action: (ConfigEvent) -> Unit) {
        listeners.add(action)
    }

    override fun getOrder(): Float = -1000f
}
