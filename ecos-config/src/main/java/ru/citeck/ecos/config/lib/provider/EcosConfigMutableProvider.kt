package ru.citeck.ecos.config.lib.provider

import ru.citeck.ecos.config.lib.dto.ConfigKey

interface EcosConfigMutableProvider : EcosConfigProvider {

    fun setConfig(key: String, value: Any?) {
        setConfig(ConfigKey.valueOf(key), value)
    }

    fun setConfig(key: ConfigKey, value: Any?)

    fun remove(key: String) {
        remove(ConfigKey.valueOf(key))
    }

    fun remove(key: ConfigKey)
}
