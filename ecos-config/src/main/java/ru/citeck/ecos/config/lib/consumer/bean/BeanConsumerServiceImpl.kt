package ru.citeck.ecos.config.lib.consumer.bean

import ecos.com.fasterxml.jackson210.databind.JavaType
import mu.KotlinLogging
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.config.lib.consumer.context.ConsumersContext
import ru.citeck.ecos.config.lib.consumer.context.ConsumersEmptyContext
import ru.citeck.ecos.config.lib.dto.ConfigKey
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KType
import kotlin.reflect.full.*
import kotlin.reflect.jvm.isAccessible
import kotlin.reflect.jvm.javaField

class BeanConsumerServiceImpl(services: EcosConfigServiceFactory) : BeanConsumerService {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    private val ecosConfigService = services.ecosConfigService
    private val properties = services.properties

    private val contexts: MutableMap<Any, ConsumersContext> = Collections.synchronizedMap(IdentityHashMap())

    override fun registerConsumers(bean: Any?): ConsumersContext {

        bean ?: return ConsumersEmptyContext

        val consumers = try {
            getConsumers(bean)
        } catch (e: Throwable) {
            log.error(e) { "Error while consumers evaluation" }
            emptyMap()
        }
        if (consumers.isNotEmpty()) {
            val context = ecosConfigService.registerConsumers(consumers)
            contexts[bean] = context
            return context
        }
        return ConsumersEmptyContext
    }

    override fun unregisterConsumers(bean: Any?) {
        bean ?: return
        contexts.remove(bean)?.remove()
    }

    override fun getConsumers(bean: Any?): Map<ConfigKey, List<(DataValue) -> Unit>> {

        bean ?: return emptyMap()

        val javaClass = bean::class.java
        if (javaClass.isSynthetic || javaClass.isAnonymousClass) {
            return emptyMap()
        }

        val result = LinkedHashMap<ConfigKey, MutableList<(DataValue) -> Unit>>()

        for (func in bean::class.functions) {
            // first argument for bean and second for value
            if (func.parameters.size != 2) {
                continue
            }
            val configAnnotation = func.findAnnotation<EcosConfig>() ?: continue
            val configKey = configAnnotation.toConfigKey()

            val argumentType = getJavaType(func.parameters[1].type)

            result.computeIfAbsent(configKey) { ArrayList() }.add(getConfigSetter(bean, argumentType, func))
        }

        for (property in bean::class.memberProperties) {

            val configAnnotation = property.findAnnotation()
                ?: property.javaField?.getAnnotation(EcosConfig::class.java)
                ?: continue

            if (property !is KMutableProperty<*>) {
                error(
                    "Property annotated with EcosConfig should be mutable. " +
                        "Bean: ${bean::class} Property: ${property.name}"
                )
            }

            val configKey = configAnnotation.toConfigKey()
            val argumentType = getJavaType(property.returnType)

            result.computeIfAbsent(configKey) { ArrayList() }.add(getConfigSetter(bean, argumentType, property.setter))
        }

        return result
    }

    private fun getConfigSetter(bean: Any, type: JavaType, callable: KCallable<*>): (DataValue) -> Unit {

        val callableInfo = "Type: $type Callable: $callable"

        callable.isAccessible = true
        if (!callable.isAccessible) {
            error("Callable is not accessible. $callableInfo")
        }

        val emptyValue = getEmptyValue(type)
        var prevValue: Any? = Unit

        return { value ->
            if (value != prevValue) {
                if (value.isNull()) {
                    if (prevValue != null) {
                        callable.call(bean, emptyValue)
                        prevValue = null
                    }
                } else {
                    val convertedValue = Json.mapper.convert<Any>(value.value, type)
                        ?: error("Config value can't be converted to required type. $callableInfo")
                    if (convertedValue != prevValue) {
                        try {
                            callable.call(bean, convertedValue)
                            prevValue = convertedValue
                        } catch (e: Throwable) {
                            log.error("Error while setter invocation. $callableInfo")
                            throw e
                        }
                    }
                }
            }
        }
    }

    private fun getEmptyValue(type: JavaType): Any? {
        if (Map::class.java.isAssignableFrom(type.rawClass)) {
            return emptyMap<Any, Any>()
        } else if (Collection::class.java.isAssignableFrom(type.rawClass)) {
            return Json.mapper.convert(emptyList<Any>(), type.rawClass)
        }
        return null
    }

    private fun getJavaType(type: KType): JavaType {
        val clazz = type.classifier as KClass<*>
        return if (Map::class.isSuperclassOf(clazz)) {
            getMapJavaType(type)
        } else if (Collection::class.isSuperclassOf(clazz)) {
            getCollectionJavaType(type)
        } else {
            Json.mapper.getType(clazz.java)
        }
    }

    private fun getMapJavaType(type: KType): JavaType {
        @Suppress("UNCHECKED_CAST")
        val mapType = (type.classifier as KClass<out Map<*, *>>).java

        if (type.arguments.size != 2) {
            error("Unexpected generic arguments length. Expected: 2 Actual: ${type.arguments.size}")
        }
        val keyType = type.arguments[0].type ?: error("First generic argument is not defined")
        val valueType = type.arguments[1].type ?: error("Second generic argument is not defined")
        return Json.mapper.getTypeFactory()
            .constructMapType(
                mapType,
                (keyType.classifier as KClass<*>).java,
                (valueType.classifier as KClass<*>).java
            )
    }

    private fun getCollectionJavaType(type: KType): JavaType {

        @Suppress("UNCHECKED_CAST")
        val collectionType = (type.classifier as KClass<out Collection<*>>).java

        if (type.arguments.size != 1) {
            error("Unexpected generic arguments length. Expected: 1 Actual: ${type.arguments.size}")
        }
        val argType = type.arguments[0].type ?: error("First generic argument is not defined")
        return Json.mapper.getTypeFactory()
            .constructCollectionType(collectionType, (argType.classifier as KClass<*>).java)
    }

    private fun EcosConfig.toConfigKey(): ConfigKey {
        return ConfigKey.valueOf(this.value)
            .withDefaultScope(properties.defaultScope)
    }
}
