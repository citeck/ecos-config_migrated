package ru.citeck.ecos.config.lib.test.provider

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.citeck.ecos.config.lib.consumer.bean.EcosConfig
import ru.citeck.ecos.config.lib.provider.EcosConfigProvider
import ru.citeck.ecos.config.lib.provider.InMemConfigService
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory

class InMemConfigServiceTest {

    lateinit var services: EcosConfigServiceFactory
    lateinit var inMemConfigService: InMemConfigService

    @BeforeEach
    fun beforeEach() {
        services = object : EcosConfigServiceFactory() {
            override fun createEcosConfigProviders(): List<EcosConfigProvider> {
                return listOf(
                    InMemConfigService(this)
                )
            }
        }
        inMemConfigService = services.ecosConfigService.getProvider(InMemConfigService::class.java)!!
    }

    @Test
    fun test() {

        val bean = TestBean()
        services.beanConsumersService.registerConsumers(bean)

        inMemConfigService.setConfig("config", "123")
        assertThat(bean.config).isEqualTo("123")

        inMemConfigService.setConfig("scope\$aaa", "scoped")
        assertThat(bean.scopedConfig).isEqualTo("scoped")
    }

    class TestBean {

        @EcosConfig("config")
        var config: String? = null

        @EcosConfig("scope\$aaa")
        var scopedConfig: String? = null
    }
}
