package ru.citeck.ecos.config.lib.test.consumer;

import ru.citeck.ecos.config.lib.consumer.bean.EcosConfig;

public class BeanConsumersJava {

    public static class JavaConfigDto {

        @EcosConfig("defaultField")
        String defaultField;
        @EcosConfig("privateField")
        private String privateField;
        @EcosConfig("protectedField")
        protected String protectedField;
        @EcosConfig("publicField")
        public String publicField;

        @EcosConfig("setDefaultField")
        void setDefaultField(String value) {
            this.defaultField = value;
        }

        @EcosConfig("setPrivateField")
        private void setPrivateField(String value) {
            this.privateField = value;
        }

        @EcosConfig("setProtectedField")
        protected void setProtectedField(String value) {
            this.protectedField = value;
        }

        @EcosConfig("setPublicField")
        public void setPublicField(String value) {
            this.publicField = value;
        }

        public String getDefaultField() {
            return defaultField;
        }

        public String getPrivateField() {
            return privateField;
        }

        public String getProtectedField() {
            return protectedField;
        }

        public String getPublicField() {
            return publicField;
        }
    }
}
