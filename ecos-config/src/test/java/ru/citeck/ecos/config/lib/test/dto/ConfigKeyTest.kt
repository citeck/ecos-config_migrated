package ru.citeck.ecos.config.lib.test.dto

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import ru.citeck.ecos.config.lib.dto.ConfigKey

class ConfigKeyTest {

    @Test
    fun test() {

        val assertToStringValueOf = { value: ConfigKey ->
            assertThat(ConfigKey.valueOf(value.toString())).isEqualTo(value)
        }

        val assertValueOf = { value: String, expScope: String, expId: String ->
            val key0 = ConfigKey.valueOf(value)
            assertThat(key0.scope).isEqualTo(expScope)
            assertThat(key0.id).isEqualTo(expId)
            assertToStringValueOf(key0)

            val key1 = ConfigKey.create(expId)
            assertThat(key1.scope).isEqualTo("")
            assertThat(key1.id).isEqualTo(expId)
            assertToStringValueOf(key1)

            val key2 = ConfigKey.create(expScope, expId)
            assertThat(key2.scope).isEqualTo(expScope)
            assertThat(key2.id).isEqualTo(expId)
            assertToStringValueOf(key2)
        }

        assertValueOf("abc\$def", "abc", "def")
        assertValueOf("\$def", "", "def")
        assertValueOf("def\$", "def", "")
        assertValueOf("abc", "", "abc")
        assertValueOf("", "", "")
        assertValueOf("$", "", "")
        assertValueOf("  aaa  \$  bbb  ", "aaa", "bbb")
        assertValueOf("abc\$def\$hig", "abc", "def\$hig")
        assertValueOf("abc\$\$def", "abc", "\$def")
        assertValueOf("abc\$def\$", "abc", "def\$")
    }
}
