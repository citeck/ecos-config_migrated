package ru.citeck.ecos.config.lib.test.consumer

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.config.lib.consumer.bean.BeanConsumerService
import ru.citeck.ecos.config.lib.consumer.bean.EcosConfig
import ru.citeck.ecos.config.lib.dto.ConfigKey
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BeanConsumerServiceTest {

    lateinit var beanConsumers: BeanConsumerService

    @BeforeAll
    fun beforeAll() {
        val services = EcosConfigServiceFactory()
        beanConsumers = services.beanConsumersService
    }

    @Test
    fun javaTest() {

        val javaDto = BeanConsumersJava.JavaConfigDto()

        val getters = mapOf(
            "defaultField" to { javaDto.getDefaultField() },
            "privateField" to { javaDto.privateField },
            "protectedField" to { javaDto.getProtectedField() },
            "publicField" to { javaDto.getPublicField() },
            "setDefaultField" to { javaDto.getDefaultField() },
            "setPrivateField" to { javaDto.privateField },
            "setProtectedField" to { javaDto.getProtectedField() },
            "setPublicField" to { javaDto.getPublicField() }
        )
        dtoTest(javaDto, getters)
    }

    @Test
    fun kotlinTest() {

        val kotlinDto = TestWithModifiers()

        val getters = mapOf(
            "defaultPropWithLateInit" to { kotlinDto.defaultPropWithLateInit },
            "defaultProp" to { kotlinDto.defaultProp },
            "privateProp" to { kotlinDto.getPrivatePropValue() },
            "protectedProp" to { kotlinDto.getProtectedPropValue() }
        )
        dtoTest(kotlinDto, getters)
    }

    private fun dtoTest(dto: Any, getters: Map<String, () -> String>) {

        val configKeys = getters.keys.map { ConfigKey.create(it) }
        val consumers = beanConsumers.getConsumers(dto)

        assertThat(consumers.keys).hasSize(getters.size)
        assertThat(consumers.keys).containsExactlyInAnyOrderElementsOf(configKeys)

        for ((idx, key) in configKeys.withIndex()) {
            val newValue = key.toString() + "_" + idx
            consumers[key]!![0].invoke(DataValue.createStr(newValue))
            assertThat(getters[key.id]!!.invoke()).describedAs(key.toString()).isEqualTo(newValue)
        }
    }

    @Test
    fun test() {

        val testInstance = TestClass()
        val consumers = beanConsumers.getConsumers(testInstance)

        assertThat(consumers).hasSize(1)
        consumers[ConfigKey.create("str-key")]!![0].invoke(DataValue.createStr("test-value"))

        assertThat(testInstance.someConfig).isEqualTo("test-value")
    }

    class TestClass {
        @EcosConfig("str-key")
        lateinit var someConfig: String
    }

    open class TestWithModifiers {

        @EcosConfig("defaultPropWithLateInit")
        lateinit var defaultPropWithLateInit: String
        @EcosConfig("defaultProp")
        var defaultProp: String = ""
        @EcosConfig("privateProp")
        private lateinit var privateProp: String
        @EcosConfig("protectedProp")
        protected lateinit var protectedProp: String

        fun getPrivatePropValue() = privateProp
        fun getProtectedPropValue() = protectedProp
    }
}
