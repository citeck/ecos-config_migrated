package ru.citeck.ecos.config.lib.test.service

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.config.lib.consumer.bean.EcosConfig
import ru.citeck.ecos.config.lib.dto.ConfigKey
import ru.citeck.ecos.config.lib.dto.ConfigValue
import ru.citeck.ecos.config.lib.dto.event.ConfigEvent
import ru.citeck.ecos.config.lib.dto.event.ConfigEventType
import ru.citeck.ecos.config.lib.provider.EcosConfigProvider
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory

class EcosConfigServiceTest {

    @Test
    fun test() {

        val providerConfigs = HashMap<String, Any>()
        var configsWatcher: ((ConfigEvent) -> Unit)? = null

        val provider = object : EcosConfigProvider {
            override fun watch(action: (ConfigEvent) -> Unit) {
                configsWatcher = action
            }
            override fun getOrder() = 0f
            override fun getConfig(key: ConfigKey): ConfigValue {
                val strKey = key.id
                return ConfigValue(DataValue.create(providerConfigs[strKey]), emptyList())
            }
        }

        val services = object : EcosConfigServiceFactory() {
            override fun createEcosConfigProviders(): List<EcosConfigProvider> {
                return listOf(provider)
            }
        }

        providerConfigs["test-prop"] = 123

        val dto = TestDto()
        services.beanConsumersService.registerConsumers(dto)

        assertThat(dto.testPropValueSetCounter).isEqualTo(1)
        assertThat(dto.testPropValue).isEqualTo("123")

        providerConfigs["test-prop"] = 456
        configsWatcher!!.invoke(ConfigEvent(ConfigKey.create("test-prop"), ConfigEventType.CHANGED))

        assertThat(dto.testPropValueSetCounter).isEqualTo(2)
        assertThat(dto.testPropValue).isEqualTo("456")
    }

    class TestDto {

        var testPropValue: String = ""
        var testPropValueSetCounter = 0

        @EcosConfig("test-prop")
        fun setTestProp(prop: String) {
            testPropValue = prop
            testPropValueSetCounter++
        }
    }
}
