package ru.citeck.ecos.config.lib.artifact.provider

import mu.KotlinLogging
import ru.citeck.ecos.apps.app.service.LocalAppService
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.config.lib.artifact.EcosConfigArtifactConstants
import ru.citeck.ecos.config.lib.dto.ConfigKey
import ru.citeck.ecos.config.lib.dto.ConfigValue
import ru.citeck.ecos.config.lib.dto.event.ConfigEvent
import ru.citeck.ecos.config.lib.provider.EcosConfigProvider
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory

class ArtifactsConfigProvider(
    services: EcosConfigServiceFactory,
    localAppService: LocalAppService
) : EcosConfigProvider {

    companion object {
        const val ORDER = 1000f

        private val log = KotlinLogging.logger {}

        private const val FIELD_ID = "id"
        private const val FIELD_SCOPE = "scope"
        private const val FIELD_VALUE = "value"
        private const val FIELD_PERMISSIONS = "permissions"
        private const val FIELD_READ = "read"
    }

    private val configs: Map<ConfigKey, ConfigValue>

    init {
        log.info { "Initialization" }

        val artifacts = localAppService.readStaticLocalArtifacts(
            EcosConfigArtifactConstants.ARTIFACT_TYPE,
            "json",
            ObjectData.create()
                .setStr("idTemplate", "\${scope|presuf('','$')}\${id}")
                .set(
                    "attsValues",
                    ObjectData.create()
                        .setStr("scope", "\${scope!\$appName{?str|presuf('app/')}}")
                )
        )

        log.info { "Found ${artifacts.size} configs" }

        val configs = HashMap<ConfigKey, ConfigValue>()
        val defaultScope = services.properties.defaultScope ?: ""
        artifacts.forEach {
            val configDef = Json.mapper.convert(it, ObjectData::class.java)
            if (configDef != null) {
                val scope = configDef.get(FIELD_SCOPE).asText().ifBlank {
                    defaultScope
                }
                val id = configDef.get(FIELD_ID).asText()
                if (id.isNotBlank()) {
                    val readPerms = configDef.get(FIELD_PERMISSIONS).get(FIELD_READ).toStrList()
                    val value = configDef.get(FIELD_VALUE)
                    configs[ConfigKey.create(scope, id)] = ConfigValue(value, readPerms)
                }
            }
        }
        this.configs = configs
    }

    override fun getConfig(key: ConfigKey): ConfigValue? {
        return configs[key]
    }

    override fun watch(action: (ConfigEvent) -> Unit) {
    }

    override fun getOrder() = ORDER
}
