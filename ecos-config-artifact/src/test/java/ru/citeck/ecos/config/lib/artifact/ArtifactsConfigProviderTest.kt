package ru.citeck.ecos.config.lib.artifact

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceProvider
import ru.citeck.ecos.apps.app.domain.artifact.source.DirectorySourceProvider
import ru.citeck.ecos.commands.CommandsServiceFactory
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import ru.citeck.ecos.config.lib.artifact.provider.ArtifactsConfigProvider
import ru.citeck.ecos.config.lib.consumer.bean.EcosConfig
import ru.citeck.ecos.config.lib.dto.EcosConfigProperties
import ru.citeck.ecos.config.lib.provider.EcosConfigProvider
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory
import ru.citeck.ecos.records3.RecordsProperties
import ru.citeck.ecos.records3.RecordsServiceFactory
import java.io.File

class ArtifactsConfigProviderTest {

    @Test
    fun test() {

        val artifactsRoot = EcosStdFile(File("./src/test/resources/eapps"))

        val ecosAppsServices = object : EcosAppsServiceFactory() {
            override fun createArtifactSourceProviders(): List<ArtifactSourceProvider> {
                return listOf(DirectorySourceProvider(artifactsRoot))
            }
        }
        ecosAppsServices.recordsServices = object : RecordsServiceFactory() {
            override fun createProperties(): RecordsProperties {
                val props = super.createProperties()
                props.appName = "test-app"
                return props
            }
        }
        ecosAppsServices.commandsServices = CommandsServiceFactory()

        val configServices = object : EcosConfigServiceFactory() {
            override fun createProperties(): EcosConfigProperties {
                val props = super.createProperties()
                props.defaultScope = "app/test-app"
                return props
            }
            override fun createEcosConfigProviders(): List<EcosConfigProvider> {
                return listOf(ArtifactsConfigProvider(this, ecosAppsServices.localAppService))
            }
        }

        val dto = TestDto()
        configServices.beanConsumersService.registerConsumers(dto)

        assertThat(dto.simpleValue).isEqualTo("simple-config-value")
        assertThat(dto.getPatchedValue()).isEqualTo("patched-value")
    }

    class TestDto {

        @EcosConfig("simple-config")
        var simpleValue: String = ""

        @EcosConfig("patch-test")
        private lateinit var patchedValue: String

        fun getPatchedValue(): String {
            return patchedValue
        }
    }
}
