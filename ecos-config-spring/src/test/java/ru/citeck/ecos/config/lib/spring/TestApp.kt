package ru.citeck.ecos.config.lib.spring

import ecos.org.apache.curator.RetryPolicy
import ecos.org.apache.curator.framework.CuratorFrameworkFactory
import ecos.org.apache.curator.retry.RetryForever
import ecos.org.apache.curator.test.TestingServer
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import ru.citeck.ecos.records3.RecordsProperties
import ru.citeck.ecos.zookeeper.EcosZooKeeper

@SpringBootApplication
open class TestApp {

    companion object {
        const val APP_NAME = "test-app"
    }

    @Bean
    @ConditionalOnMissingBean(RecordsProperties::class)
    open fun recordsProps(): RecordsProperties {
        val props = RecordsProperties()
        props.appName = APP_NAME
        return props
    }

    @Bean
    @ConditionalOnMissingBean(EcosZooKeeper::class)
    open fun ecosZookeeper(): EcosZooKeeper {

        val zkServer = TestingServer()

        val retryPolicy: RetryPolicy = RetryForever(7_000)
        val client = CuratorFrameworkFactory
            .newClient(zkServer.connectString, retryPolicy)
        client.start()

        return EcosZooKeeper(client).withNamespace("ecos")
    }
}
