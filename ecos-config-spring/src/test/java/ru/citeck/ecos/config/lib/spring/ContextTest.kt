package ru.citeck.ecos.config.lib.spring

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.config.lib.consumer.bean.EcosConfig
import ru.citeck.ecos.config.lib.provider.InMemConfigService
import ru.citeck.ecos.config.lib.zookeeper.ZkConfigValue
import ru.citeck.ecos.zookeeper.EcosZooKeeper

@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [TestApp::class])
class ContextTest {

    @Autowired
    lateinit var ecosZooKeeper: EcosZooKeeper

    @Autowired
    lateinit var inMemConfigService: InMemConfigService

    @EcosConfig("test-config")
    lateinit var configField: String

    @EcosConfig("patched-config")
    lateinit var patchedValue: String

    @Test
    fun test() {

        assertThat(configField).isEqualTo("test-config-value")

        val value = ZkConfigValue(DataValue.createStr("new-value"), emptyList())
        ecosZooKeeper.withNamespace("")
            .setValue("/ecos/config/app/${TestApp.APP_NAME}/test-config", value)
        Thread.sleep(500)

        assertThat(configField).isEqualTo("new-value")

        inMemConfigService.setConfig("test-config", "new-value-2")
        assertThat(configField).isEqualTo("new-value-2")

        inMemConfigService.remove("test-config")
        assertThat(configField).isEqualTo("new-value")

        assertThat(patchedValue).isEqualTo("value-after-patch")
    }
}
