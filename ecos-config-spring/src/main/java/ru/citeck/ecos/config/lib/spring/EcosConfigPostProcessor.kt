package ru.citeck.ecos.config.lib.spring

import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.stereotype.Component
import ru.citeck.ecos.config.lib.consumer.bean.BeanConsumerService

@Component
class EcosConfigPostProcessor(
    private val beanConsumerService: BeanConsumerService
) : BeanPostProcessor {

    override fun postProcessBeforeInitialization(bean: Any, beanName: String): Any {
        if ((bean::class.qualifiedName ?: "").startsWith("ru.citeck.ecos")) {
            beanConsumerService.registerConsumers(bean)
        }
        return bean
    }
}
