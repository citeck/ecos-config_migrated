package ru.citeck.ecos.config.lib.spring.config

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

/**
 * Auto configuration to initialize records beans.
 *
 * @author Roman Makarskiy
 */
@Configuration
@ComponentScan(basePackages = ["ru.citeck.ecos.config.lib.spring"])
open class EcosConfigAutoConfiguration
