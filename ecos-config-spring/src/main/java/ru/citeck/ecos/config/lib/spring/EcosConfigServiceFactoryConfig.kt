package ru.citeck.ecos.config.lib.spring

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.config.lib.artifact.provider.ArtifactsConfigProvider
import ru.citeck.ecos.config.lib.consumer.bean.BeanConsumerService
import ru.citeck.ecos.config.lib.dto.EcosConfigProperties
import ru.citeck.ecos.config.lib.provider.EcosConfigProvider
import ru.citeck.ecos.config.lib.provider.InMemConfigService
import ru.citeck.ecos.config.lib.records.CfgCtxAttsProvider
import ru.citeck.ecos.config.lib.records.CfgRecordsDao
import ru.citeck.ecos.config.lib.service.EcosConfigService
import ru.citeck.ecos.config.lib.service.EcosConfigServiceFactory
import ru.citeck.ecos.config.lib.zookeeper.ZkConfigService
import ru.citeck.ecos.records3.RecordsServiceFactory
import ru.citeck.ecos.zookeeper.EcosZooKeeper
import javax.annotation.PostConstruct

@Configuration
open class EcosConfigServiceFactoryConfig(
    private val ecosAppsServiceFactory: EcosAppsServiceFactory,
    private val recordsServiceFactory: RecordsServiceFactory,
    private val ecosZooKeeper: EcosZooKeeper
) : EcosConfigServiceFactory() {

    private var customProperties: EcosConfigProperties? = null
    private var customProviders: List<EcosConfigProvider> = emptyList()

    @PostConstruct
    fun init() {
        recordsServiceFactory.ctxAttsService.register(CfgCtxAttsProvider(this))
        recordsServiceFactory.recordsServiceV1.register(CfgRecordsDao(this))
    }

    @Bean
    override fun createBeanConsumersService(): BeanConsumerService {
        return super.createBeanConsumersService()
    }

    @Bean
    override fun createEcosConfigService(): EcosConfigService {
        return super.createEcosConfigService()
    }

    override fun createProperties(): EcosConfigProperties {
        val props = customProperties ?: EcosConfigProperties()
        val appName = recordsServiceFactory.properties.appName
        if (props.defaultScope.isNullOrBlank() && appName.isNotBlank()) {
            props.defaultScope = "app/$appName"
        }
        return props
    }

    @Bean
    open fun createZkConfigService(): ZkConfigService {
        return ZkConfigService(ecosZooKeeper, this)
    }

    @Bean
    override fun createInMemConfigService(): InMemConfigService {
        return super.createInMemConfigService()
    }

    override fun createEcosConfigProviders(): List<EcosConfigProvider> {
        val providers = mutableListOf(
            createZkConfigService(),
            ArtifactsConfigProvider(this, ecosAppsServiceFactory.localAppService)
        )
        providers.addAll(customProviders)
        return providers
    }

    @Autowired(required = false)
    fun setProperties(properties: EcosConfigProperties) {
        customProperties = properties
    }

    @Autowired(required = false)
    fun setConfigProviders(providers: List<EcosConfigProvider>) {
        this.customProviders = providers
    }
}
